const express = require('express');
const cors = require('cors');
//const bodyParser = require('body-parser');
const auth = require('../routes/auth');
const users = require('../routes/users');
const powerPlants = require('../routes/power-plants');
const states = require('../routes/states');
const error = require('../middleware/error');

module.exports = function (app) {
	app.use(express.json());
	app.use(cors())
	//app.use(bodyParser.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded
	app.use('/api/auth', auth);
	app.use('/api/users', users);
	app.use('/api/power-plants', powerPlants);
	app.use('/api/states', states);
	//Always put the error handler middleware function at the end of every middlewares.
	app.use(error);
};
