const Joi = require('joi');
// const uniqueValidator = require('mongoose-unique-validator');
const mongoose = require('mongoose');

const stateSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        minLength: 2,
        maxLength: 100,
    },
    code: {
        type: String,
        required: true,
        minLength: 2,
        maxLength: 50,
    }
});

//stateSchema.plugin(uniqueValidator);
const State = mongoose.model('State', stateSchema);

const getStates = async () => {
    return await State.find().sort('name');
};

const getStateById = async (id) => {
    return await State.findById(id);
};

const getStateByName = async (name) => {
    return await State.findOne({ name: { $regex: new RegExp("^" + name.toLowerCase(), "i") } });
};

const createState = async (data) => {
    const state = new State({
        name: data.name,
        code: data.code
    });

    await state.save();

    return state;
};

const createManyStates = async (arrData) => {
    return State.insertMany(arrData)
};

const updateState = async (id, data) => {
    return await State.findByIdAndUpdate(
        id,
        {
            $set: {
                name: data.name,
                code: data.code,
            }
        },
        { new: true }
    );
};

const deleteState = async (id) => {
    return await State.findByIdAndDelete(id);
};

function validateState(state) {
    const schema = Joi.object({
        name: Joi.string().min(2).max(100).required(),
        code: Joi.string().min(2).max(100).required(),
    });

    return schema.validate(state);
}

module.exports.State = State;
module.exports.validateState = validateState;
module.exports.stateModel = {
    getStates,
    getStateById,
    createState,
    updateState,
    deleteState,
    getStateByName,
    createManyStates,
    stateSchema
};
