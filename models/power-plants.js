const Joi = require('joi');
const _ = require('lodash');
const mongoose = require('mongoose');
// const uniqueValidator = require('mongoose-unique-validator');

const powerPlantSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxLength: 255,
    },
    genntan: {
        type: Number,
        default: 0,
    },
    code: {
        type: String,
        maxLength: 50,
    },
    percentage: {
        type: Number,
        maxLength: 50,
    },
    isActive: {
        type: Boolean, default: true
    },
});

// powerPlantSchema.plugin(uniqueValidator, {
//     message: 'Error, expected {PATH} to be unique.'
// });

const PowerPlant = mongoose.model('PowerPlant', powerPlantSchema);

const getPowerPlants = async () => {
    return await PowerPlant.find().sort('name');
};

const getPowerPlantById = async (id) => {
    return await PowerPlant.findById(id);
};

const createPowerPlant = async (data) => {
    const powerPlant = new PowerPlant({
        name: data.name,
        genntan: data.genntan,
        code: data.code,
        percentage: data.percentage,
        isActive: data.isActive,
    });

    await powerPlant.save();

    return powerPlant;
};

const getPowerPlantByName = async (name) => {
    return await PowerPlant.findOne({ name: name });
};

const getPowerPlantsByState = async (sa) => {
    return await PowerPlant.find({
        code: sa
    });
};

const createManyPowerPlants = async (arrData) => {
    return PowerPlant.insertMany(arrData)
};

const updatePowerPlant = async (id, data) => {
    return await PowerPlant.findOneAndUpdate(
        id,
        {
            $set: {
                name: data.name,
                genntan: data.genntan,
                code: data.code,
                percentage: data.percentage,
                isActive: data.isActive,
            }
        },
        { new: true }
    );
};

const deletePowerPlant = async (id) => {
    return await PowerPlant.findByIdAndDelete(id);
};

function validatePowerPlant(powerPlant) {
    const schema = Joi.object({
        name: Joi.string().max(255).required(),
        genntan: Joi.number(),
        percentage: Joi.number(),
        code: Joi.string().max(50).required(),
        isActive: Joi.boolean(),
    });

    return schema.validate(powerPlant);
}

module.exports.PowerPlant = PowerPlant;
module.exports.validatePowerPlant = validatePowerPlant;
module.exports.powerPlantModel = {
    getPowerPlants,
    getPowerPlantByName,
    getPowerPlantsByState,
    getPowerPlantById,
    createPowerPlant,
    updatePowerPlant,
    deletePowerPlant,
    createManyPowerPlants,
    powerPlantSchema
};
