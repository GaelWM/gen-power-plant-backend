const mongodb = require("mongodb").MongoClient;
const csvtojson = require("csvtojson");
const states_json = require("../states.json");

let url = "mongodb://localhost:27017/";
csvtojson()
    .fromFile("GEN20.csv")
    .then(csvData => {
        let results = csvData.map(d => {
            let genntan = d["Generator ozone season net generation (MWh)"];
            genntan = (typeof genntan === 'undefined' || isNaN(genntan)) ? 0 : Math.abs(genntan);
            code = d["Plant state abbreviation"];
            return { name: d["Plant name"], genntan: genntan, code: code, isActive: true }
        });

        results = results.filter(r => r.code !== 'PSTATABB');
        let totalGR = sum('genntan', results);

        let result = [];
        results.reduce(function (res, value) {
            if (!res[value.code]) {
                res[value.code] = { code: value.code, genntan: 0, name: value.name, isActive: value.isActive, percentage: 0 };
                result.push(res[value.code])
            }
            res[value.code].genntan += value.genntan;
            return res;
        }, {});


        result.map(r => {
            r.percentage = ((r.genntan / totalGR) * 100).toFixed(2);
            return r;
        })


        // console.log('result: ', result);
        mongodb.connect(
            url,
            { useNewUrlParser: true, useUnifiedTopology: true },
            (err, client) => {
                if (err) throw err;
                client.db("power_plant").collection("powerplants").deleteMany({})
                client
                    .db("power_plant")
                    .collection("powerplants")
                    .insertMany(result, (err, res) => {
                        if (err) throw err;
                        console.log(`Power Plants - Inserted: ${res.insertedCount} rows`);
                        client.close();
                    });
            }
        );
    });

mongodb.connect(
    url,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, client) => {
        if (err) throw err;
        client.db("power_plant").collection("states").deleteMany({})
        client
            .db("power_plant")
            .collection("states")
            .insertMany(states_json, (err, res) => {
                if (err) throw err;
                console.log(`Inserted: ${res.insertedCount} rows`);
                client.close();
            });
    }
);

function sum(key, array = []) {
    return array.reduce((a, b) => a + (b[key] || 0), 0)
}

