# GenPowerPlantBackend

This project is the backend of the gen-power-plant-webapp

## Prerequisite

- Install Mongo DB Compass
- Install Node > 16.1
- Depending on the OS you are running, please follow this tutorial and install & run mongo server https://docs.mongodb.com/manual/installation/
- Make sure you run the Mongo DB server on a default port

## Cloning & Starting Project

Clone the gen-power-plant-backend project.

- Run npm install
- Run export power_plant_jwtPrivateKey=12345 on Mac
- Run set power_plant_jwtPrivateKey=12345 on Windows
- Make sure mongo db is running on port 27017
- Run npm start
- Run node utils/fileimport.js
- Make sure the backend is running on port localhost:3000

## End project screenshots in root folder

- Please check the end project screenshot to have a look what to expect

  - gpp-showing-AK-specific.png
  - gpp-showing-all-states.png
  - gpp-showing-top-5-plants.png
