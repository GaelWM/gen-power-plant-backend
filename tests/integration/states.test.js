const { User } = require('../../models/users');
const { State } = require('../../models/states');
const request = require('supertest');
const mongoose = require('mongoose');

let server;
let token;

describe('/api/states/', () => {
    beforeEach(() => {
        server = require('../../index');
        token = new User().getAuthenticationToken();
    });

    afterEach(async () => {
        await State.deleteMany({})
        await server.close();
    });

    describe('GET /', () => {
        afterEach(async () => {
            await State.deleteMany({})
            await server.close();
        });

        it('should return 401 if user is not logged in', async () => {
            token = '';
            const res = await request(server).get('/api/states').set('x-auth-token', token);
            expect(res.status).toBe(401);
        });

        it('should return all states', async () => {
            await State.collection.insertMany([{ name: 'AK' }, { name: 'AL' }]);
            const res = await request(server).get('/api/states').set('x-auth-token', token);
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body.some((g) => g.name === 'AK')).toBeTruthy();
            expect(res.body.some((g) => g.name === 'AL')).toBeTruthy();
        });

        it('should return a state if a valid id is passed', async () => {
            const state = new State({ name: 'AK' });
            await state.save();
            const res = await request(server).get('/api/states/' + state._id).set('x-auth-token', token);
            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('name', state.name);
        });
    });

    describe('POST /', () => {
        let name;

        beforeEach(() => {
            name = 'AK';
        });

        afterEach(async () => {
            await State.deleteMany({})
            await server.close();
        });

        const exec = () => {
            return request(server).post('/api/states').set('x-auth-token', token).send({ name: name });
        };

        it('should return 401 if user is not logged in', async () => {
            token = '';
            const res = await request(server).get('/api/states').set('x-auth-token', token);
            expect(res.status).toBe(401);
        });

        it('should return 400 if state has less than 3 characters', async () => {
            name = 'uv';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('should return 400 if state has more than 50 characters', async () => {
            name = new Array(52).join('a');
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('should create a new state if valid input is passed', async () => {
            await exec();
            const state = await State.find({ name: 'AK' });
            expect(state).not.toBeNull();
        });

        it('should return state if it is valid', async () => {
            const res = await exec();
            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name', 'AK');
        });

    });

    describe('UPDATE /', () => {
        let name;
        let stateId;

        beforeEach(() => {
            stateId = mongoose.Types.ObjectId().toHexString();
            name = 'My AK';
        });

        afterEach(async () => {
            await State.deleteMany({})
            await server.close();
        });

        const exec = (stateId) => {
            return request(server).put('/api/states/' + stateId).set('x-auth-token', token).send({ name: name });
        };

        it('should return 401 if user is not logged in', async () => {
            token = '';

            const res = await exec(stateId);
            expect(res.status).toBe(401);
        });

        it('should return 404 if id is not valid', async () => {
            const stateId = 'red';
            const res = await exec(stateId);
            expect(res.status).toBe(404);
        });

        it('should return 400 if state has less than 2 characters', async () => {
            name = 'u';
            const res = await exec(stateId);
            expect(res.status).toBe(400);
        });

        it('should return 400 if genre has more than 50 characters', async () => {
            name = new Array(52).join('a');
            const res = await exec(stateId);
            expect(res.status).toBe(400);
        });

        it('should update a state if valid input is passed', async () => {
            const state = new State({ name: 'AK' });
            await state.save();
            await exec(state._id);
            const updatedState = await State.findOne({ name: 'My AK' });
            expect(updatedState).not.toBeNull();
            expect(updatedState.name).toContain('My AK');
        });

    });

    describe('DELETE /', () => {
        let stateId;

        beforeEach(() => {
            stateId = mongoose.Types.ObjectId().toHexString();
            name = 'My AK';
        });

        afterEach(async () => {
            await State.deleteMany({})
            await server.close();
        });

        const exec = (stateId) => {
            return request(server).delete('/api/states/' + stateId).set('x-auth-token', token);
        };

        it('should return 401 if user is not logged in', async () => {
            token = '';
            const res = await exec(stateId);
            expect(res.status).toBe(401);
        });

        it('should return 404 if id is not valid', async () => {
            const stateId = 'red';
            const res = await exec(stateId);
            expect(res.status).toBe(404);
        });

        it('should return 404 if state is not found', async () => {
            const res = await exec(stateId);
            expect(res.status).toBe(404);
        });

        it('should delete a state if valid input is passed', async () => {
            const state = new State({ name: 'AK' });
            await state.save();
            const res = await exec(state._id);
            expect(res.body).not.toBeNull();
            expect(res.body.name).toContain('AK');
        });
    });
});
