const { User } = require('../../models/users');
const { PowerPlant } = require('../../models/power-plants');
const request = require('supertest');
const mongoose = require('mongoose');

let server;
let token;

describe('/api/power-plants/', () => {
    beforeEach(() => {
        server = require('../../index');
        token = new User().getAuthenticationToken();
    });

    afterEach(async () => {
        await PowerPlant.deleteMany({})
        await server.close();
    });

    describe('GET /', () => {
        afterEach(async () => {
            await PowerPlant.deleteMany({})
            await server.close();
        });

        it('should return 401 if user is not logged in', async () => {
            token = '';
            const res = await request(server).get('/api/power-plants').set('x-auth-token', token);
            expect(res.status).toBe(401);
        });

        it('should return all power-plants', async () => {
            await PowerPlant.collection.insertMany([{ name: 'AED' }, { name: 'USD' }]);
            const res = await request(server).get('/api/power-plants').set('x-auth-token', token);
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body.some((g) => g.name === 'AED')).toBeTruthy();
            expect(res.body.some((g) => g.name === 'USD')).toBeTruthy();
        });

        it('should return a power plant if a valid id is passed', async () => {
            const powerPlant = new PowerPlant({ name: 'AED' });
            await powerPlant.save();
            const res = await request(server).get('/api/power-plants/' + powerPlant._id).set('x-auth-token', token);
            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('name', powerPlant.name);
        });
    });

    describe('POST /', () => {
        let name;
        let unitDollarRate;

        beforeEach(() => {
            const user = { _id: mongoose.Types.ObjectId().toHexString(), isAdmin: true }
            token = new User(user).getAuthenticationToken();
            name = 'AED';
            unitDollarRate = 3.67;
        });

        afterEach(async () => {
            await PowerPlant.deleteMany({})
            await server.close();
        });

        const exec = () => {
            return request(server).post('/api/power-plants').set('x-auth-token', token)
                .send({ name: name, unitDollarRate: unitDollarRate });
        };

        it('should return 400 if input is not valid', async () => {
            unitDollarRate = 'xxx';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('should create a new power plant if valid input is passed', async () => {
            await exec();
            const powerPlant = await PowerPlant.findOne({ name: 'AED' });
            expect(powerPlant).not.toBeNull();
            expect(powerPlant).toHaveProperty('name', 'AED');
        });
    });

    describe('UPDATE /', () => {
        let name;
        let unitDollarRate;
        let powerPlantId;

        beforeEach(() => {
            const user = { _id: mongoose.Types.ObjectId().toHexString(), isAdmin: true }
            token = new User(user).getAuthenticationToken();
            powerPlantId = mongoose.Types.ObjectId().toHexString();
            name = 'CFC';
            unitDollarRate = 1750;
        });

        afterEach(async () => {
            await PowerPlant.deleteMany({});
            await server.close();
        });

        const exec = (powerPlantId) => {
            return request(server).put('/api/power-plants/' + powerPlantId)
                .set('x-auth-token', token).send({ name: name, unitDollarRate: unitDollarRate });
        };

        it('should return 401 if user is not logged in', async () => {
            token = '';
            const res = await exec(powerPlantId);
            expect(res.status).toBe(401);
        });

        it('should return 403 if user is not an admin', async () => {
            const user = { _id: mongoose.Types.ObjectId().toHexString() }
            token = new User(user).getAuthenticationToken();
            const res = await exec(powerPlantId);
            expect(res.status).toBe(403);
        });

        it('should return 404 if id is not valid', async () => {
            const powerPlantId = 'red';
            const res = await exec(powerPlantId);
            expect(res.status).toBe(404);
        });

        it('should update a power plant if valid input is passed', async () => {
            const powerPlant = new PowerPlant({ name: 'USD' });
            await powerPlant.save();
            const res = await exec(powerPlant._id);
            const updatedCategory = await PowerPlant.findOne({ name: 'CFC' });
            expect(res.status).toBe(200);
            expect(updatedCategory).not.toBeNull();
            expect(updatedCategory).toHaveProperty('name', 'CFC');
        });
    });

    describe('DELETE /', () => {
        let powerPlantId;

        beforeEach(() => {
            const user = { _id: mongoose.Types.ObjectId().toHexString(), isAdmin: true }
            token = new User(user).getAuthenticationToken();
            powerPlantId = mongoose.Types.ObjectId().toHexString();
        });

        afterEach(async () => {
            await PowerPlant.deleteMany({})
            await server.close();
        });

        const exec = (powerPlantId) => {
            return request(server).delete('/api/power-plants/' + powerPlantId).set('x-auth-token', token);
        };

        it('should return 401 if user is not logged in', async () => {
            token = '';
            const res = await exec(powerPlantId);
            expect(res.status).toBe(401);
        });

        it('should return 403 if user is not an admin', async () => {
            const user = { _id: mongoose.Types.ObjectId().toHexString() }
            token = new User(user).getAuthenticationToken();
            const res = await exec(powerPlantId);
            expect(res.status).toBe(403);
        });

        it('should return 404 if id is not valid', async () => {
            const powerPlantId = 'red';
            const res = await exec(powerPlantId);
            expect(res.status).toBe(404);
        });

        it('should return 404 if power plant is not found', async () => {
            const res = await exec(powerPlantId);
            expect(res.status).toBe(404);
        });

        it('should delete a power plant if valid input is passed', async () => {
            const powerPlant = new PowerPlant({ name: 'CFC' });
            await powerPlant.save();
            const res = await exec(powerPlant._id);
            expect(res.status).toBe(200);
            expect(res.body).not.toBeNull();
            expect(res.body).toHaveProperty('name', 'CFC');
        });
    });
});
