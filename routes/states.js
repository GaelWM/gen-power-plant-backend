const validateObjectId = require('../middleware/validateObjectId');
// const authMiddleware = require('../middleware/auth');
const { validateState, stateModel } = require('../models/states');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    const categories = await stateModel.getStates();
    res.send(categories);
});

router.post('/', async (req, res) => {
    let state = await stateModel.getStateByName(req.body.name);
    if (state) return res.status(404).send('The state already exist.');

    const { error } = validateState(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    state = await stateModel.createState(req.body);
    return res.send(state);
});

router.post('/many/', async (req, res) => {
    state = await stateModel.createManyStates(req.body);
    return res.send(state);
});

router.get('/:id', [validateObjectId], async (req, res) => {
    const state = await stateModel.getStateById(req.params.id);
    if (!state) return res.status(404).send('The state with the given id was not found');
    return res.send(state);
});

router.put('/:id', [validateObjectId], async (req, res) => {
    const { error } = validateState(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const state = await stateModel.updateState(req.params.id, req.body);
    if (!state) return res.status(404).send('The state with the given id was not found');

    return res.send(state);
});

router.delete('/:id', [validateObjectId], async (req, res) => {
    const state = await stateModel.getStateById(req.params.id);
    if (!state) return res.status(404).send('The state with the given id was not found');

    await stateModel.deleteState(req.params.id);
    return res.send(state);
});

module.exports = router;
