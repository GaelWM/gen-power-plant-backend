const validateObjectId = require('../middleware/validateObjectId');
// const validatePowerPlantId = require('../middleware/validatePowerPlantId');
const authMiddleware = require('../middleware/auth');
// const adminMiddleware = require('../middleware/admin');
const express = require('express');
const { validatePowerPlant, powerPlantModel } = require('../models/power-plants');
const router = express.Router();

router.get('/', async (req, res) => {
    const powerPlants = await powerPlantModel.getPowerPlants();
    res.send(powerPlants);
});

router.post('/', async (req, res) => {
    const exist = await powerPlantModel.getPowerPlantByName(req.body.name);
    if (exist) return res.status(404).send('The power plant already exist.');

    const { error } = validatePowerPlant(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const powerPlant = await powerPlantModel.createPowerPlant(req.body);
    return res.send(powerPlant);
});

router.get('/:id', [validateObjectId], async (req, res) => {
    const powerPlant = await powerPlantModel.getPowerPlantById(req.params.id);
    if (!powerPlant) return res.status(404).send('The power plant with the given id was not found');
    return res.send(powerPlant);
});

router.get('/state/:state', async (req, res) => {
    const powerPlant = await powerPlantModel.getPowerPlantsByState(req.params.state);
    if (!powerPlant) return res.status(404).send('Power plants with the given state were not found.');
    return res.send(powerPlant);
});

router.put('/:id', [validateObjectId], async (req, res) => {

    const exist = await powerPlantModel.getPowerPlantByName(req.body.name);
    if (exist) return res.status(404).send('The power plant already exist.');

    const { error } = validatePowerPlant(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const powerPlant = await powerPlantModel.updatePowerPlant(req.params.id, req.body);
    if (!powerPlant) return res.status(404).send('The power plant item with the given id was not found');

    return res.send(powerPlant);
});

router.delete('/:id', [validateObjectId], async (req, res) => {
    const powerPlant = await powerPlantModel.getPowerPlantById(req.params.id);
    if (!powerPlant) return res.status(404).send('The power plant with the given id was not found');

    await powerPlantModel.deletePowerPlant(req.params.id);

    return res.send(powerPlant);
});

module.exports = router;

